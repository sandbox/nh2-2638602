<?php
/**
 * @file
 * Admin Settings form.
 */
namespace Drupal\eucl\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EuclSettingForm.
 * @package Drupal\eucl\Form
 */
class EuclSettingForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eucl_admin_config';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return array(
      'eucl.config',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $popup_settings = $this->getPopupConfig();

    $form['popup_message'] = array(
      '#type' => 'text_format',
      '#title' => t('Popup message'),
      '#default_value' => isset($popup_settings['popup_message']['value']) ? $popup_settings['popup_message']['value'] : '',
      '#required' => TRUE,
      '#format' => isset($popup_settings['popup_message']['format']) ? $popup_settings['popup_message']['format'] : filter_default_format(),
    );
    $form['accept_button_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Accept button title'),
      '#default_value' => isset($popup_settings['accept_button_title']) ? $popup_settings['accept_button_title'] : '',
      '#required' => TRUE,
    );
    $form['more_infos_title'] = array(
      '#type' => 'textfield',
      '#title' => t('More infos title'),
      '#default_value' => isset($popup_settings['more_infos_title']) ? $popup_settings['more_infos_title'] : '',
      '#required' => TRUE,
    );
    $form['more_infos_link'] = array(
      '#type' => 'textfield',
      '#title' => t('More infos link'),
      '#default_value' => isset($popup_settings['more_infos_link']) ? $popup_settings['more_infos_link'] : '',
      '#required' => TRUE,
    );


    return parent::buildForm($form, $form_state);
  }

  /**
   * Retrieves settings from the database for a current language.
   *
   * @return array|mixed|null
   *
   */
  private function getPopupConfig() {
    $language = \Drupal::languageManager()->getCurrentLanguage();
    $language = $language->getId();
    $popup_settings = \Drupal::config('eucl.settings')->get($language);
    return $popup_settings;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $language = \Drupal::languageManager()->getCurrentLanguage();
    $language = $language->getId();
    $values = $form_state->getValues();
    $config = \Drupal::configFactory()->getEditable("eucl.settings");
    $config
      ->set($language, $values)
      ->save();
    drupal_set_message(t('Your changes have been successfully saved.'));
  }
}
