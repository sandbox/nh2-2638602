/**
 * JS file handling the cookie acceptance and display
 */
"use strict";
jQuery(function () {
  jQuery('.accept-eucl').on('click', function (e) {
    e.preventDefault();
    jQuery.cookie('EUCL_ACCEPT_HTTPS', '1', {path: '/', secure: true, expires: 365});
    jQuery.cookie('EUCL_ACCEPT', '1', {path: '/', secure: false, expires: 365});
    jQuery('div.cookie').slideUp('slow');
  });

  if (jQuery.cookie('EUCL_ACCEPT') || jQuery.cookie('EUCL_ACCEPT_HTTPS')) {
    jQuery('div.cookie').hide();
  }
});
