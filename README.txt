
-- SUMMARY --

Module to display a popup to comply with EU Cookie Law


-- REQUIREMENTS --

    - core/jquery
    - core/drupal
    - core/drupalSettings
    - core/jquery.once
    - core/jquery.cookie

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

* You likely want to disable Toolbar module, since its output clashes with
  Administration menu.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions:

  - Administer site configuration

* Customize the EUCL popup  settings in Administration
  "Administration > Configuration > EUCL Compliance "



-- CUSTOMIZATION --

* To customize the display of your popup,
 add the following line to your theme's stylesheet:

  .cookie {
      background: #444;
      color: #fff;
      font-size: 15px;
      padding: 10px 0;
      position: relative;
      width: 100%;
      text-align: center;
      -moz-transition-property: all;
      -o-transition-property: all;
      -webkit-transition-property: all;
      transition-property: all;
      -moz-transition-duration: .3s;
      -o-transition-duration: .3s;
      -webkit-transition-duration: .3s;
      transition-duration: .3s;
      -moz-transition-timing-function: ease-in;
      -o-transition-timing-function: ease-in;
      -webkit-transition-timing-function: ease-in;
      transition-timing-function: ease-in;
  }

  .cookie .btn {
      padding: 2px 10px;
      font-weight: bold;
      margin: 0 20px;
      background-color: #4b85c5;
      border-radius: 0;
      border: 0;
      color: #fff;
  }

  .cookie a {
      color: #4b85c5;
      text-decoration: underline;
  }


-- CONTACT --

Current maintainer:
* Amine MOKEDDEM (nh2) - https://www.drupal.org/u/nh2

This project has been sponsored by:
* WEBNET
  Visit http://www.webnet.fr for more information.
